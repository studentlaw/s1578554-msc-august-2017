#!/usr/bin/env python
#
# Copyright (c) 2017 Andy Law
#                 <s1578554@sms.ed.ac.uk>, <andy.law@roslin.ed.ac.uk>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# --
# Python script to take a list of chromosome lengths from a file specified
# on the command line and distribute a number of markers and genetic linkage map
# space across them proportionately to their size.
#
# build_params.py -c <chromosomefile> -m markerCount -g genomeLength
#
import getopt
import sys


def read_chromosome_file(filename):
    """
    Read in a chromosome file from disk.
    Return an array of relative chromosome sizes
    The values in the array should add up to one as they represent the
    proportions of the total genome size

    :param filename: Path to the file of chromosome lengths
    :return: List of chromosome lengths as a proportion of the whole genome
    :rtype: List
    """
    with open(filename) as f:
        content = f.readlines()
    # remove whitespace characters
    content = [x.strip() for x in content]
    content_num = [float(i) for i in content]
    length = sum(content_num)
    return [x / length for x in content_num]


def print_genome_section(proportions, genome_length, marker_count, qtl_count):
    # type: (List, int, int, int) -> None
    """

    :rtype: None
    :param proportions: List of chromosome lengths as a proportion of the 
    whole genome
    :param genome_length: Length of the genome in cM 
    :param marker_count: Total number of markers in the genome
    :param qtl_count: Total number of QTL in the genome
    """
    chromosome = 1
    MAX_PER_CHROM = 400000
    overspill_fraction = 1.0
    overspill = 0
    print '''
    /*******************************
     **          Genome           **
     *******************************/
     begin_genome;'''.strip()
    # For each proportion value in turn
    for val in proportions:
        chrlen = max(int(val * genome_length), 1)
        # calculate a base allocation and a proportionate overspill allocation
        base_allocation = int(val * marker_count)
        overspill_allocation = int((val/overspill_fraction) * overspill)
        # if we exceeded the max number on the base allocation alone, then the overspill needs to grow,
        # the number of loci gets set to the maximum and we strip down the remaining proportion that we
        # need to share the overspill between
        if base_allocation > MAX_PER_CHROM:
            overspill += base_allocation - MAX_PER_CHROM
            nmloci = MAX_PER_CHROM
            overspill_fraction -= val
        # if we go over the maximum only by including a block of the overspill then the overspill needs
        # to shrinkg. Again the number of loci gets seto the maximum and we strip down the remaining
        # proportion of the genome over which we need to share the overspill
        elif (base_allocation + overspill_allocation) > MAX_PER_CHROM:
            overspill -= (MAX_PER_CHROM - base_allocation)
            nmloci = MAX_PER_CHROM
            overspill_fraction -= val
        # otherwise just allocate out the base set plus any overspill and carry on
        else:
            nmloci = base_allocation + overspill_allocation
        nqloci = int(val * qtl_count)
        print '''

   begin_chr = 1;
      chrlen = %3s;           //Chromosome %s length
      nmloci = %3s;           //Number of markers
      mpos   = rnd;           //Marker positions
      nma    = all 2;         //Number of marker alleles
      maf    = eql;           //Marker allele frequencies
      nqloci = %2s;            //Number of QTL
      qpos   = rnd;           //QTL positions
      nqa    = rnd 2 3 4;     //Number of QTL alleles
      qaf    = eql;           //QTL allele frequencies
      qae    = rndg 0.4;      //QTL allele effects
   end_chr;
'''.strip() % (chrlen, chromosome, nmloci, nqloci)
        chromosome += 1
    print '''
    end_genome;'''.strip()

def main(argv):
    chromfile = ''
    marker_count = 1000
    genome_length = 3000
    qtl_count = 700
    try:
        opts, args = getopt.getopt(argv, "hg:c:m:",
                                   ["chrom_file=", "marker_count=",
                                       "genome_length="])
    except getopt.GetoptError:
        print 'build_params.py -c <chromosomefile> -m markerCount -g ' \
              'genomeLength'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'build_params.py -c <chromosomefile> -m markerCount'
            sys.exit()
        elif opt in ("-c", "--chrom_file"):
            chromfile = arg
        elif opt in ("-m", "--marker_count"):
            marker_count = int(arg)
        elif opt in ("-g", "--genome_length"):
            genome_length = int(arg)

    try:
    	proportions = read_chromosome_file(chromfile)

    	print_genome_section(proportions, genome_length, marker_count, qtl_count)
    
    except:
        print 'build_params.py -c <chromosomefile> -m markerCount -g ' \
              'genomeLength'
        sys.exit(2)


if __name__ == "__main__":
    main(sys.argv[1:])
