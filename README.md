# Welcome

This repository contains key data files and scripts relating to the Dissertation
submitted to Edinburgh University by Andrew Stephen Law in partial fulfilment of the
requirements of the degree of Master of Science in High Performance Computing with
Data Science.

Within the repository, files, scripts and README files are distributed in folders that
correspond to the relevant chapter in the thesis.
