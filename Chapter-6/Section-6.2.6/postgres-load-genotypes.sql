-- 
-- Copyright (c) 2017 Andy Law
-- 				   (<s1578554@sms.ed.ac.uk>, <andy.law@roslin.ed.ac.uk>)
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
-- 
-- ----------------------------------------------- 
BEGIN;
-- --------------------------------------------------
-- SQL Script to load Genotype data from the simulation program output into PostgreSQL
--
-- The script assumes that the following data files have been placed
-- into a 'data-for-import' directory. Before running the script, it
-- must first be passed through a filter to replace the placeholder text
-- '<<BASEDIR>>' with the path to the location of the data-for-import
-- directory e.g.
--
--  cat postgres-load-genotypes.sql | sed -e "s#<<BASEDIR>>#/path/to/directory#"
--
--     genotypes.txt
-- --------------------------------------------------

-- Turn on Timing
\timing on

-- create an alias for simple sizing
\set tsize 'SELECT nspname || \'.\' || relname AS \"relation\", pg_relation_size(C.oid) AS "size" FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace) WHERE nspname NOT IN (\'pg_catalog\', \'information_schema\', \'pg_toast\') ORDER BY relation;'


-- --------------------------------------------------
-- Load the genotype data. Because this is such a big lump of data, we create the 
-- index before loading
--

CREATE TABLE genotype (
	individual_id CHARACTER VARYING(7) NOT NULL,
	marker_name CHARACTER VARYING(8) NOT NULL,
	genotype CHARACTER VARYING(2) NOT NULL
);

-- --------------------------------------------------
-- Index the Genotype table
--
CREATE INDEX genotype_to_marker_individual_idx
ON genotype(marker_name, individual_id);


COPY genotype (
	individual_id,
	marker_name,
	genotype
)
FROM  '<<BASEDIR>>/data-for-import/genotypes.txt'
WITH (
	FORMAT CSV
);


\echo 'BLOCK - end genotype load'

-- Turn timing off (so we can process the output more easily)
\timing off

COMMIT;

:tsize
