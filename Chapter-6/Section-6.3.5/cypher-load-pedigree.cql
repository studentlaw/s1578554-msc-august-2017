:begin
// 
// Copyright (c) 2017 Andy Law
// 				   (<s1578554@sms.ed.ac.uk>, <andy.law@roslin.ed.ac.uk>)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 
// --------------------------------------------------
// Cypher Script to load Pedigree data from the simulation program output into Neo4J
//
// The script assumes that the following data files have been placed
// into the 'import' sub-directory of the Neo4J instance
//
//     pedigree.txt
// --------------------------------------------------


// --------------------------------------------------
// Load the Pedigree data and the Phenotype information
//

return "Starting";

LOAD CSV WITH HEADERS FROM "file:///pedigree.txt" AS line
CREATE (
	p:Individual {
		id: line.`Progeny`, 
		sex: line.`Sex`,
		f: line.`F`,
		homo: line.`Homo`,
		phen: line.`Phen`,
		res: line.`Res`,
		polygene: line.`Polygene`,
		qtl: line.`QTL`,
		sireid: line.`Sire`,
		damid: line.`Dam`
	}
);


:commit

CREATE INDEX ON :Individual(id);

CALL db.awaitIndex(":Individual(id)", 20);

:begin


// Fill in Sire relationships

MATCH (a:Individual), (s:Individual)
WHERE a.sireid = s.id
MERGE (s)-[:IS_SIRE_OF]->(a);


// Fill in Dam relationships

MATCH (a:Individual), (d:Individual)
WHERE a.damid = d.id
MERGE (d)-[:IS_DAM_OF]->(a);

return "loaded basic pedigree...";

:commit
