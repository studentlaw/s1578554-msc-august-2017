-- 
-- Copyright (c) 2017 Andy Law
-- 				   (<s1578554@sms.ed.ac.uk>, <andy.law@roslin.ed.ac.uk>)
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
-- 
-- ----------------------------------------------- 
BEGIN;
-- --------------------------------------------------
-- SQL Script to load Pedigree data from the simulation program output into PostgreSQL
--
-- The script assumes that the following data files have been placed
-- into a 'data-for-import' directory. Before running the script, it
-- must first be passed through a filter to replace the placeholder text
-- '<<BASEDIR>>' with the path to the location of the data-for-import
-- directory e.g.
--
--  cat postgres-load-pedigree.sql | sed -e "s#<<BASEDIR>>#/path/to/directory#"
--
--     pedigree.txt
-- --------------------------------------------------

-- Turn on Timing
\timing on

-- create an alias for simple sizing
\set tsize 'SELECT nspname || \'.\' || relname AS \"relation\", pg_relation_size(C.oid) AS "size" FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace) WHERE nspname NOT IN (\'pg_catalog\', \'information_schema\', \'pg_toast\') ORDER BY relation;'

-- --------------------------------------------------
-- Load the Pedigree data and the Phenotype information


CREATE TABLE individual (
	id CHARACTER VARYING(7) PRIMARY KEY,
	sire_id CHARACTER VARYING(7),
	dam_id CHARACTER VARYING(7),
	sex CHARACTER(1),
	g SMALLINT,
	nmprg SMALLINT,
	nfprg SMALLINT,
	f REAL,
	homo REAL,
	phen REAL,
	res REAL,
	polygene REAL,
	qtl REAL
);


COPY individual (
	id,
	sire_id,
	dam_id,
	sex,
	g,
	nmprg,
	nfprg,
	f,
	homo,
	phen,
	res,
	polygene,
	qtl
)
FROM  '<<BASEDIR>>/data-for-import/pedigree.txt'
WITH (
	HEADER TRUE,
	FORMAT CSV
);


CREATE INDEX individual_sire_idx
ON individual(sire_id);

CREATE INDEX individual_dam_idx
ON individual(dam_id);

\echo 'BLOCK - end pedigree load'

-- Turn timing off (so we can process the output more easily)
\timing off

COMMIT;

:tsize
