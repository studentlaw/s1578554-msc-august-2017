# Pre-processing marker data

Marker data was converted using awk with the following command:

```
cat lm_mrk_001.txt | awk 'BEGIN {OFS=","} {print $1, $2, $3}' > markers.txt
```
