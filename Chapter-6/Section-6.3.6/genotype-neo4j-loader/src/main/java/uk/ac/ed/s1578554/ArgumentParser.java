/**
 *
 * Copyright (c) 2017 Andy Law
 *                     (<s1578554@sms.ed.ac.uk>, <andy.law@roslin.ed.ac.uk>)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package uk.ac.ed.s1578554;

import java.util.HashMap;

/**
 *
 * @author Andy Law (<s1578554@sms.ed.ac.uk>, <andy.law@roslin.ed.ac.uk>)
 *
 */
public class ArgumentParser {

    public static final String FILE_PATH_PARAMETER = "file";
    public static final String START_INDIVIDUAL_PARAMETER = "startIndividual";
    public static final String END_INDIVIDUAL_PARAMETER = "endIndividual";
    public static final String START_MARKER_PARAMETER = "startMarker";
    public static final String END_MARKER_PARAMETER = "endMarker";
    public static final String CODE_GENOTYPE_AS_PARAMETER = "codeGenotypeAs";

    public static final String CODE_GT_AS_PROPERTY_VALUE
            = GenotypeCodings.PROPERTIES.toString();
    public static final String CODE_GT_AS_RELATIONSHIP_VALUE
            = GenotypeCodings.RELATIONSHIP.toString();
    public static final String CODE_GT_AS_NULL
            = GenotypeCodings.NONE.toString();

    private final String filename;
    private final int startIndividualLine;
    private final int endIndividualLine;
    private final int startMarkerIndex;
    private final int endMarkerIndex;
    private final GenotypeCodings genotypeCodedAs;

    /**
     * Get the value of filename
     *
     * @return the value of filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * Get the value of startIndividualLine
     *
     * @return the value of startIndividualLine
     */
    public int getStartIndividualLine() {
        return startIndividualLine;
    }

    /**
     * Get the value of endIndividualLine
     *
     * @return the value of endIndividualLine
     */
    public int getEndIndividualLine() {
        return endIndividualLine;
    }

    /**
     * Get the value of startMarkerIndex
     *
     * @return the value of startMarkerIndex
     */
    public int getStartMarkerIndex() {
        return startMarkerIndex;
    }

    /**
     * Get the value of endMarkerIndex
     *
     * @return the value of endMarkerIndex
     */
    public int getEndMarkerIndex() {
        return endMarkerIndex;
    }

    /**
     * @return the genotypeCodedAs
     */
    public GenotypeCodings getGenotypeCodedAs() {
        return genotypeCodedAs;
    }

    public ArgumentParser(HashMap<String, Object> input) {

        this.filename
                = (String) input.get(FILE_PATH_PARAMETER);

        this.startIndividualLine
                = (input.containsKey(START_INDIVIDUAL_PARAMETER))
                ? (int) input.get(START_INDIVIDUAL_PARAMETER)
                : 1;

        this.endIndividualLine
                = (input.containsKey(END_INDIVIDUAL_PARAMETER))
                ? (int) input.get(END_INDIVIDUAL_PARAMETER)
                : Integer.MAX_VALUE;

        this.startMarkerIndex
                = (input.containsKey(START_MARKER_PARAMETER))
                ? (int) input.get(START_MARKER_PARAMETER) - 1
                : 0;

        this.endMarkerIndex
                = (input.containsKey(END_MARKER_PARAMETER))
                ? (int) input.get(END_MARKER_PARAMETER) - 1
                : Integer.MAX_VALUE;

        String gtCodeVal = CODE_GT_AS_PROPERTY_VALUE; // default is properties
        if (input.containsKey(CODE_GENOTYPE_AS_PARAMETER)) {
            gtCodeVal = (String) input.get(CODE_GENOTYPE_AS_PARAMETER);
        }
        GenotypeCodings gtCodeMethod = GenotypeCodings.getGenotypeCoding(gtCodeVal);
        if (gtCodeMethod == null) {
            gtCodeMethod = GenotypeCodings.PROPERTIES; // default is properties
        }
        this.genotypeCodedAs = gtCodeMethod;
    }

}
