/**
 *
 * Copyright (c) 2017 Andy Law
 *                     (<s1578554@sms.ed.ac.uk>, <andy.law@roslin.ed.ac.uk>)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package uk.ac.ed.s1578554;

import org.codehaus.jackson.map.ObjectMapper;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Andy Law (<s1578554@sms.ed.ac.uk>, <andy.law@roslin.ed.ac.uk>)
 *
 */
@Path("/import")
public class Import {

    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final String ID_PROPERTY = "id";
    private static final String NAME_PROPERTY = "name";

    @POST
    @Path("/genotypes")
    public Response importGenotypes(String body, @Context GraphDatabaseService db) throws Exception {
        int count = 0;
        int lineNumber = 0;

// Build an ArgumentParser
        ArgumentParser args;
        HashMap<String, Object> input;
        try {
            input = objectMapper.readValue(body, HashMap.class);
            args = new ArgumentParser(input);
        } catch (IOException e) {
//            throw Exceptions.invalidInput;
            throw new Exceptions(400, e.getLocalizedMessage());
        }

        long[] markers = null;

        int startIndividual = args.getStartIndividualLine();
        int endIndividual = args.getEndIndividualLine();

        int startMarker = args.getStartMarkerIndex();
        int endMarker = args.getEndMarkerIndex();

// Make us a GenotypeBuilder object
        GenotypeBuilder gtBuilder = new WithPropertyGenotypeBuilder();
        GenotypeCodings val = args.getGenotypeCodedAs();
        switch (val) {
// - default so we don't need to specify
//            case PROPERTIES:
//                gtBuilder = new WithPropertyGenotypeBuilder();
//                break;
            case RELATIONSHIP:
                gtBuilder = new NoPropertyGenotypeBuilder();
                break;
            case NONE:
                gtBuilder = new NoGenotypeBuilder();
                break;
        }

        Instant startLoad;
        Instant start = Instant.now();
        try (BufferedReader br = new BufferedReader(new FileReader(args.getFilename()))) {
            // Skip lines until we find the first Individual that we want to work with.
            // We can do using a less-than comparison because the first line is 
            // header line
            while (lineNumber < startIndividual) {
                br.readLine();
                lineNumber++;
            }

            startLoad = Instant.now();

            Transaction tx = db.beginTx();
            try {
                for (String line; (line = br.readLine()) != null;) {
                    // If we've done all the individuals that we need then 
                    // break out from the file-reading loop
                    if (lineNumber > endIndividual) {
                        break;
                    }
                    lineNumber++;

                    String[] split = line.split("\\s+");

                    // Going to pre-create all the Markers and load them into an array for un-indexed access later.
                    if (markers == null) {
                        endMarker = Math.min(endMarker, split.length / 2);

                        markers = new long[split.length / 2];
                        for (int i = startMarker; i <= endMarker; i++) {
                            String markerName = "M" + (i + 1);
                            Node marker = db.findNode(Labels.Marker, NAME_PROPERTY, markerName);
                            if (marker == null) {
                                marker = db.createNode(Labels.Marker);
                                marker.setProperty(NAME_PROPERTY, markerName);
                            }
                            markers[i] = marker.getId();

                            if (i % 1000 == 0) {
                                tx.success();
                                tx.close();
                                tx = db.beginTx();
                            }
                        }

                    }

                    Node individual = db.findNode(Labels.Individual, ID_PROPERTY, split[0]);
                    if (individual == null) {
                        individual = db.createNode(Labels.Individual);
                        individual.setProperty(ID_PROPERTY, split[0]);
                    }
                    //
                    for (int i = startMarker; i <= endMarker; i++) {
                        Node marker = db.getNodeById(markers[i]);
                        // it's + 1 because first item is individual id
                        String a1 = split[(i * 2) + 1];
                        String a2 = split[(i * 2) + 2];
                        gtBuilder.BuildGenotype(individual, marker, a1, a2);
                        count++;
                        if (count % 1000 == 0) {
                            tx.success();
                            tx.close();
                            tx = db.beginTx();
                        }
                    }
                }

                tx.success();
            } finally {
                tx.close();
            }

        }
        Instant end = Instant.now();

        Map<String, Object> results = new HashMap<>();
        results.put("Imported Genotypes Count", count);
        results.put("Time to skip lines", Duration.between(start, startLoad).toMillis());
        results.put("Time taken to load", Duration.between(startLoad, end).toMillis());

        return Response.ok().entity(objectMapper.writeValueAsString(results)).build();
    }

}
