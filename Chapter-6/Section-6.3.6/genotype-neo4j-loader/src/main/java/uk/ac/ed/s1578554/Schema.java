/**
 *
 * Copyright (c) 2017 Andy Law
 *                     (<s1578554@sms.ed.ac.uk>, <andy.law@roslin.ed.ac.uk>)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package uk.ac.ed.s1578554;

import org.codehaus.jackson.map.ObjectMapper;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Andy Law (<s1578554@sms.ed.ac.uk>, <andy.law@roslin.ed.ac.uk>)
 *
 */
@Path("/schema")
public class Schema {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @POST
    @Path("/create")
    public Response create(@Context GraphDatabaseService db) throws IOException {
        ArrayList<String> results = new ArrayList<>();

        try (Transaction tx = db.beginTx()) {
            org.neo4j.graphdb.schema.Schema schema = db.schema();
            if (!schema.getConstraints(Labels.Individual).iterator().hasNext()) {
                schema.constraintFor(Labels.Individual)
                        .assertPropertyIsUnique("id")
                        .create();
                tx.success();
                results.add("(:Individual {id}) constraint created");
            }
        }
        try (Transaction tx = db.beginTx()) {
            org.neo4j.graphdb.schema.Schema schema = db.schema();
            if (!schema.getConstraints(Labels.Marker).iterator().hasNext()) {
                schema.constraintFor(Labels.Marker)
                        .assertPropertyIsUnique("name")
                        .create();
                tx.success();
                results.add("(:Marker {name}) constraint created");
            }
        }
        results.add("Schema Created");

        return Response.ok().entity(objectMapper.writeValueAsString(results)).build();
    }
}
