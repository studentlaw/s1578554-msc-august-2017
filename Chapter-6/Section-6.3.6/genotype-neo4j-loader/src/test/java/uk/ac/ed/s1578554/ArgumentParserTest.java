/**
 *
 * Copyright (c) 2017 Andy Law
 *                     (<s1578554@sms.ed.ac.uk>, <andy.law@roslin.ed.ac.uk>)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
package uk.ac.ed.s1578554;

import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Andy Law (<s1578554@sms.ed.ac.uk>, <andy.law@roslin.ed.ac.uk>)
 *
 */
public class ArgumentParserTest {

    protected HashMap<String, Object> inputArgs;

    private static final int DEFAULT_START_INDIVIDUAL_LINE = 1;
    private static final int DEFAULT_END_INDIVIDUAL_LINE = Integer.MAX_VALUE;
    private static final int DEFAULT_START_MARKER_INDEX = 0;
    private static final int DEFAULT_END_MARKER_INDEX = Integer.MAX_VALUE;
    private static final boolean DEFAULT_GENOTYPE_CODED_AS_RELATIONSHIP = false;

    public ArgumentParserTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        inputArgs = new HashMap();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test Default Filename values of class ArgumentParser.
     */
    @Test
    public void testDefaultFilename() {
        ArgumentParser instance = new ArgumentParser(inputArgs);
        String result = instance.getFilename();
        assertNull("Should get null value for filename", result);
    }

    /**
     * Test Get Filename values of class ArgumentParser.
     */
    @Test
    public void testGetFilename() {
        final String expected = "imaginary/path/to/file";
        inputArgs.put(ArgumentParser.FILE_PATH_PARAMETER, expected);
        ArgumentParser instance = new ArgumentParser(inputArgs);
        String result = instance.getFilename();
        assertEquals("Should get supplied value for filename", expected, result);
    }

    /**
     * Test Default StartIndividualLine values of class ArgumentParser.
     */
    @Test
    public void testDefaultStartIndividualLine() {
        ArgumentParser instance = new ArgumentParser(inputArgs);
        int expResult = DEFAULT_START_INDIVIDUAL_LINE;
        int result = instance.getStartIndividualLine();
        assertEquals("Should get default value for startIndividualLine", expResult, result);
    }

    /**
     * Test Get StartIndividualLine values of class ArgumentParser.
     */
    @Test
    public void testGetStartIndividualLine() {
        final Integer expResult = 99;
        inputArgs.put(ArgumentParser.START_INDIVIDUAL_PARAMETER, expResult);
        ArgumentParser instance = new ArgumentParser(inputArgs);
        int result = instance.getStartIndividualLine();
        assertEquals("Should get supplied value for startIndividualLine", (long) expResult, result);
    }

    /**
     * Test Default EndIndividualLine values of class ArgumentParser.
     */
    @Test
    public void testDefaultEndIndividualLine() {
        ArgumentParser instance = new ArgumentParser(inputArgs);
        int expResult = DEFAULT_END_INDIVIDUAL_LINE;
        int result = instance.getEndIndividualLine();
        assertEquals("Should get default value for endIndividualLine", expResult, result);
    }

    /**
     * Test Get EndIndividualLine values of class ArgumentParser.
     */
    @Test
    public void testGetEndIndividualLine() {
        final Integer expResult = 7654;
        inputArgs.put(ArgumentParser.END_INDIVIDUAL_PARAMETER, expResult);
        ArgumentParser instance = new ArgumentParser(inputArgs);
        int result = instance.getEndIndividualLine();
        assertEquals("Should get supplied value for startIndividualLine", (long) expResult, result);
    }

    /**
     * Test Default StartMarkerIndex values of class ArgumentParser.
     */
    @Test
    public void testDefaultStartMarkerIndex() {
        ArgumentParser instance = new ArgumentParser(inputArgs);
        int expResult = DEFAULT_START_MARKER_INDEX;
        int result = instance.getStartMarkerIndex();
        assertEquals("Should get default value for startMarkerIndex", expResult, result);
    }

    /**
     * Test Get StartMarkerIndex values of class ArgumentParser.
     */
    @Test
    public void testGetStartMarkerIndex() {
        final Integer setValue = 4116;
        final Integer expResult = setValue - 1;
        inputArgs.put(ArgumentParser.START_MARKER_PARAMETER, setValue);
        ArgumentParser instance = new ArgumentParser(inputArgs);
        int result = instance.getStartMarkerIndex();
        assertEquals("Should get supplied value for startMarkerIndex", (long) expResult, result);
    }

    /**
     * Test Default EndMarkerIndex values of class ArgumentParser.
     */
    @Test
    public void testDefaultEndMarkerIndex() {
        ArgumentParser instance = new ArgumentParser(inputArgs);
        int expResult = DEFAULT_END_MARKER_INDEX;
        int result = instance.getEndMarkerIndex();
        assertEquals("Should get default value for endMarkerIndex", expResult, result);
    }

    /**
     * Test Get EndMarkerIndex values of class ArgumentParser.
     */
    @Test
    public void testGetEndMarkerIndex() {
        final Integer setValue = 13;
        final Integer expResult = setValue - 1;
        inputArgs.put(ArgumentParser.END_MARKER_PARAMETER, setValue);
        ArgumentParser instance = new ArgumentParser(inputArgs);
        int result = instance.getEndMarkerIndex();
        assertEquals("Should get supplied value for endMarkerIndex", (long) expResult, result);
    }

    /**
     * Test Default GenotypeCodedAs values of class ArgumentParser.
     */
    @Test
    public void testDefaultGenotypeCodedAs() {
        ArgumentParser instance = new ArgumentParser(inputArgs);
        GenotypeCodings expResult = GenotypeCodings.PROPERTIES;
        GenotypeCodings result = instance.getGenotypeCodedAs();
        assertEquals("Should get default value for genotypeCodedAs", expResult, result);
    }

    /**
     * Test Get GenotypeCodedAs values of class ArgumentParser.
     */
    @Test
    public void testGetGenotypeCodedAsRelationship() {
        final GenotypeCodings expResult = GenotypeCodings.RELATIONSHIP;
        inputArgs.put(ArgumentParser.CODE_GENOTYPE_AS_PARAMETER, expResult.toString());
        ArgumentParser instance = new ArgumentParser(inputArgs);
        GenotypeCodings result = instance.getGenotypeCodedAs();
        assertEquals("Should get supplied value (Relationship) for genotypeCodedAs", expResult, result);
    }

    /**
     * Test Get GenotypeCodedAs values of class ArgumentParser.
     */
    @Test
    public void testGetGenotypeCodedAsNone() {
        final GenotypeCodings expResult = GenotypeCodings.NONE;
        inputArgs.put(ArgumentParser.CODE_GENOTYPE_AS_PARAMETER, expResult.toString());
        ArgumentParser instance = new ArgumentParser(inputArgs);
        GenotypeCodings result = instance.getGenotypeCodedAs();
        assertEquals("Should get supplied value (None) for genotypeCodedAs", expResult, result);
    }

    /**
     * Test Get GenotypeCodedAs values of class ArgumentParser.
     */
    @Test
    public void testGetGenotypeCodedAsProperties() {
        final GenotypeCodings expResult = GenotypeCodings.PROPERTIES;
        inputArgs.put(ArgumentParser.CODE_GENOTYPE_AS_PARAMETER, expResult.toString());
        ArgumentParser instance = new ArgumentParser(inputArgs);
        GenotypeCodings result = instance.getGenotypeCodedAs();
        assertEquals("Should get supplied value (Properties) for genotypeCodedAs", expResult, result);
    }

    /**
     * Test Get GenotypeCodedAsRelationship values of class ArgumentParser.
     */
    @Test
    public void testGetGenotypeCodedAsFake() {
        final String fakeValue = "Fake";
        GenotypeCodings expResult = GenotypeCodings.PROPERTIES;
        inputArgs.put(ArgumentParser.CODE_GENOTYPE_AS_PARAMETER, fakeValue);
        ArgumentParser instance = new ArgumentParser(inputArgs);
        GenotypeCodings result = instance.getGenotypeCodedAs();
        assertEquals("Should get default value (Properties) for genotypeCodedAsRelationship", expResult, result);
    }

}
