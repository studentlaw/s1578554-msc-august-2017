# Java Code

To compile this code, load it into a suitable IDE (we recommend NetBeans 8.2 or later) or
run the command
```
mvn clean install
```

in the top level of the source code tree (the directory containing the file called `pom.xml`)