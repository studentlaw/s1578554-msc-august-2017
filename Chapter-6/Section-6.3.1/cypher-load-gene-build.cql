:begin
// 
// Copyright (c) 2017 Andy Law
// 				   (<s1578554@sms.ed.ac.uk>, <andy.law@roslin.ed.ac.uk>)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 
// --------------------------------------------------
// Cypher Script to load Gene Build and related data into Neo4J
//
// The script assumes that the following data files have been placed
// into the 'import' sub-directory of the Neo4J instance
//
//     chromosomes-with-lengths.txt
//     genes.txt
//     genes-to-chromosomes.txt
//     transcripts.txt
//     transcripts-to-chromosomes.txt
//     transcripts-to-genes.txt
//     transcript-5prime-ends.txt
//     transcript-3prime-ends.txt
//     exons.txt
//     exons-to-chromosomes.txt
//     exons-to-transcripts.txt
//     genes-to-goterms.txt
// --------------------------------------------------


// --------------------------------------------------
// Load the Chromosome list and index it
//

return "Starting";

LOAD CSV WITH HEADERS FROM "file:///chromosomes-with-lengths.txt" AS line
CREATE (
	:Chromosome {
		index: toInteger(line.`Index`),
		name: line.`Chromosome`,
		length: toInteger(toFloat(line.`Length`) * 1000000),
		genetic_length: toInteger(line.`Cmlength`)
	}
);

:commit


:begin

CREATE INDEX ON :Chromosome(name);

CREATE INDEX ON :Chromosome(index);

:commit

:begin

// --------------------------------------------------
// Wait for the indexes to become available
//

CALL db.awaitIndex(":Chromosome(name)", 10);

CALL db.awaitIndex(":Chromosome(index)", 10);

return "loaded chromosomes";

:commit


:begin
// --------------------------------------------------
// Load the Gene list and index it
//

LOAD CSV WITH HEADERS FROM "file:///genes.txt" AS line
CREATE (
	:Gene {
		id: line.`Gene stable ID`,
		name: line.`Gene name`,
		description: line.`Gene description`,
		type: line.`Gene type`
	}
);

:commit


:begin
CREATE INDEX ON :Gene(id);

:commit


:begin
// --------------------------------------------------
// Wait for the indexes to become available
//

CALL db.awaitIndex(":Gene(id)", 10);
return "loaded genes";

:commit


:begin
// --------------------------------------------------
// Map the Genes to the Chromosomes
//

LOAD CSV WITH HEADERS FROM "file:///genes-to-chromosomes.txt" AS line
MATCH (
	chromosome:Chromosome {
		name: line.`Chromosome/scaffold name`
	}
)
MATCH (
	gene:Gene {
		id: line.`Gene stable ID`
	}
)
MERGE
	(gene)-[
		:MAPS_TO {
			start: toInteger(line.`Gene start (bp)`),
			end: toInteger(line.`Gene end (bp)`),
			strand: toInteger(line.Strand)
		}
	]->(chromosome);

return "loaded genes to chromosomes";

:commit


:begin
// --------------------------------------------------
// Load the transcript ids and names
//

LOAD CSV WITH HEADERS FROM "file:///transcripts.txt" AS line
CREATE (
	:Transcript {
		id: line.`Transcript stable ID`,
		name: line.`Transcript name`
	}
);

:commit


:begin
CREATE INDEX ON :Transcript(id);

:commit


:begin
// Wait for the index to become available

CALL db.awaitIndex(":Transcript(id)", 10);

return "loaded transcripts";

:commit


:begin
// --------------------------------------------------
// Map the transcripts to chromosomes
//

LOAD CSV WITH HEADERS FROM "file:///transcripts-to-chromosomes.txt" AS line
MATCH (
	chromosome:Chromosome {
		name: line.`Chromosome/scaffold name`
	}
)
MATCH (
	transcript:Transcript {
		id: line.`Transcript stable ID`
	}
)
MERGE (
	transcript)-[
		:MAPS_TO {
			start: toInteger(line.`Transcript start (bp)`),
			end: toInteger(line.`Transcript end (bp)`),
			strand: toInteger(line.Strand)
		}
	]->(chromosome);

return "loaded transcripts to chromosomes";

:commit


:begin
// --------------------------------------------------
// Map the transcripts to genes
//

LOAD CSV WITH HEADERS FROM "file:///transcripts-to-genes.txt" AS line
MATCH (
	gene:Gene {
		id: line.`Gene stable ID`
	}
)
MATCH (
	transcript:Transcript {
		id: line.`Transcript stable ID`
	}
)
MERGE (transcript)-[:IS_TRANSCRIPT_FOR]->(gene);

return "mapped transcripts to genes";

:commit


:begin
// --------------------------------------------------
// Define the 5' UTR ends of the transcripts
//

LOAD CSV WITH HEADERS FROM "file:///transcript-5prime-ends.txt" AS line
MATCH (
	transcript:Transcript {
		id: line.`Transcript stable ID`
	}
)
MATCH (
	chromosome:Chromosome {
		name: line.`Chromosome/scaffold name`
	}
)
CREATE (fivep:Five_prime_utr)
MERGE (transcript)-[:HAS_5P_UTR]->(fivep)
MERGE (
	fivep)-[
		:MAPS_TO {
			start: toInteger(line.`5' UTR start`),
			end: toInteger(line.`5' UTR end`),
			strand: toInteger(line.Strand)
		}
	]->(chromosome);

return "loaded and mapped 5'UTR to transcripts and chromosomes";

:commit


:begin
// --------------------------------------------------
// Define the 3' UTR ends of the transcripts
//

LOAD CSV WITH HEADERS FROM "file:///transcript-3prime-ends.txt" AS line
MATCH (
	transcript:Transcript {
		id: line.`Transcript stable ID`
	}
)
MATCH (
	chromosome:Chromosome {
		name: line.`Chromosome/scaffold name`
	}
)
CREATE (threep:Three_prime_utr)
MERGE (transcript)-[:HAS_3P_UTR]->(threep)
MERGE (
	threep)-[
		:MAPS_TO {
			start: toInteger(line.`3' UTR start`),
			end: toInteger(line.`3' UTR end`),
			strand: toInteger(line.Strand)
		}
	]->(chromosome);

return "loaded and mapped 3'UTR to transcripts and chromosomes";

:commit


:begin
// --------------------------------------------------
// Load the exons
//

LOAD CSV WITH HEADERS FROM "file:///exons.txt" AS line
CREATE (
	:Exon {
		id: line.`Exon stable ID`
	}
);

:commit


:begin
CREATE INDEX ON :Exon(id);

:commit


:begin
// Wait for the index to become available

CALL db.awaitIndex(":Exon(id)", 10);

return "loaded exons";
:commit


:begin
// --------------------------------------------------
// Map the exons to chromosomes
//

LOAD CSV WITH HEADERS FROM "file:///exons-to-chromosomes.txt" AS line
MATCH (
	chromosome:Chromosome {
		name: line.`Chromosome/scaffold name`
	}
)
MATCH (
	exon:Exon {
		id: line.`Exon stable ID`
	}
)
MERGE (
	exon)-[
		:MAPS_TO {
			start: toInteger(line.`Exon region start (bp)`),
			end: toInteger(line.`Exon region end (bp)`),
			strand: toInteger(line.Strand)
		}
	]->(chromosome);
return "mapped exons to chromosomes";

:commit


:begin
// --------------------------------------------------
// Map the exons to transcripts
//

LOAD CSV WITH HEADERS FROM "file:///exons-to-transcripts.txt" AS line
MATCH (
	transcript:Transcript {
		id: line.`Transcript stable ID`
	}
)
MATCH (
	exon:Exon {
		id: line.`Exon stable ID`
	}
)
MERGE (
	exon)-[
		:MAKES_TRANSCRIPT {
			rank: toInteger(line.`Exon rank in transcript`)
		}
	]->(transcript);
return "mapped exons to transcripts";

:commit

