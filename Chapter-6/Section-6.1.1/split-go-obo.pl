#!/usr/bin/perl -w
#
# Copyright (c) 2017 Andy Law
#                 <s1578554@sms.ed.ac.uk>, <andy.law@roslin.ed.ac.uk>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# --
#
# Perl script to split the GO-OBO file into blocks for importing into Neo4J/Postgres
#
# Part of the work submitted in respect of the MSc in HPC with Data Science
#
# Script expects the go-basic-obo.txt file as input on STDIN and will generate
# output files with hard-coded names in the directory within which it is run
#

use strict;
use warnings;

my ($id, $description, $name, $namespace) = ("","","","");
my $collecting = 0;

open TERMS, ">", "go-terms.txt" or die "Opening the go-terms.txt file";
open ALIASES, ">", "go-aliases.txt" or die "Opening the go-aliases.txt file";
open IS_A, ">", "go-is_a.txt" or die "Opening the go-is_a.txt file";
open PART_OF, ">", "go-part_of.txt" or die "Opening the go-part_of.txt file";
open REGULATES, ">", "go-regulates.txt" or die "Opening the go-regulates.txt file";

while (defined (my $line = <>)) {
	chomp ($line);

# if we see a new term or typedef then dump our previous record (assuming that we
# have an id to work from
	if ($line =~ m/^\[/) {
		if ($id ne "") {
			print TERMS join(",", $id, $description, $name, $namespace), "\n";
		}
		$id = $description = $name = $namespace = "";
		$collecting = 0;
		if ($line =~ m/^\[Term\]/) {
			$collecting = 1;
		}
		next;
	}

# If we are not currently in a [Term] block then just skip
	next unless ($collecting);

# if it looks like a data line, then process it
	if ($line =~ m/^([a-z_]+:)\s+(.*)$/) {

		my ($token, $rest) = ($1, $2);

# ID line? store the id
		if ($token eq "id:") {
			$id = $rest;
		}
		
# Namespace line? store it
		elsif ($token eq "namespace:") {
			$namespace = $rest;
		}
		
# Name line? store it (wrapped in quotes to hide commas)
		elsif ($token eq "name:") {
			$name = '"' . $rest . '"';
		}
		
# Definition line? store it (only retaining the bits between double quotes)
		elsif ($token eq "def:") {
			if ($rest =~ m/^(.*")\s*\[[^"]+\](\s+\{comment=.*\}\s*)*$/) {
				$description = $1;
				# Throw away any accidental newline characters !!
				$description =~ s/\\n//g;
				# deal with a situation where the description ends with a "" which seems
				# to break postgres import
				if ($description =~ m/\\""/) {
					$description =~ s/\\"//g;
				}
			}
		}
		
# alt_id line? Dump the alternate id relationship to a separate place, having
# first created an empty record for the alternate ID
		elsif ($token eq "alt_id:") {
			print TERMS join(",", $rest, "", "", "alias"), "\n";
			print ALIASES join(",", $rest, $id), "\n";
		}
		
# is_a line? extract the target ID and dump a row to the IS_A relationship file
		elsif ($token eq "is_a:") {
			my ($target) = split(" ", $rest);
			print IS_A join(",", $id, $target), "\n";
		}
		
# relationship line? extract the target ID and type
		elsif ($token eq "relationship:") {
			my ($type,$target) = split(" ", $rest);

# if it's a part_of relationship then dump a row to the PART_OF relationship file
			if ($type eq "part_of") {
				print PART_OF join(",", $id, $target), "\n";
			}
# if it's a regulates relationship then dump a row to the REGULATES relationship
# file, including the type for later processing
			else {
				print REGULATES join(",", $id, $target, $type), "\n";
			}
		}
	}
	
}

close TERMS;
close ALIASES;
close IS_A;
close PART_OF;
close REGULATES;

