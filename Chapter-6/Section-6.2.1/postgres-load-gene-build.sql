-- 
-- Copyright (c) 2017 Andy Law
-- 				   (<s1578554@sms.ed.ac.uk>, <andy.law@roslin.ed.ac.uk>)
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
-- 
-- ----------------------------------------------- 
BEGIN;
-- --------------------------------------------------
-- SQL Script to load Gene Build and related data into PostgreSQL
--
-- The script assumes that the following data files have been placed
-- into a 'data-for-import' directory. Before running the script, it
-- must first be passed through a filter to replace the placeholder text
-- '<<BASEDIR>>' with the path to the location of the data-for-import
-- directory e.g.
--
--  cat postgres-load-gene-build.sql | sed -e "s#<<BASEDIR>>#/path/to/directory#"
--
--     chromosomes-with-lengths.txt
--     genes.txt
--     genes-to-chromosomes.txt
--     transcripts.txt
--     transcripts-to-chromosomes.txt
--     transcripts-to-genes.txt
--     transcript-5prime-ends.txt
--     transcript-3prime-ends.txt
--     exons.txt
--     exons-to-chromosomes.txt
--     exons-to-transcripts.txt
-- --------------------------------------------------

-- Turn on Timing
\timing on

-- create an alias for simple sizing
\set tsize 'SELECT nspname || \'.\' || relname AS \"relation\", pg_relation_size(C.oid) AS "size" FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace) WHERE nspname NOT IN (\'pg_catalog\', \'information_schema\', \'pg_toast\') ORDER BY relation;'

-- --------------------------------------------------
-- Load the Chromosome list and index it
--
-- Note that we have to load into a temporary table and then 
-- transform the length coding


CREATE TEMPORARY TABLE tchromosome (
	id INTEGER,
	name CHARACTER VARYING(5),
	length REAL,
	genetic_length INTEGER
);


COPY tchromosome (
	id,
	name,
	length,
	genetic_length
)
FROM  '<<BASEDIR>>/data-for-import/chromosomes-with-lengths.txt'
WITH (
	HEADER TRUE,
	FORMAT CSV
);

CREATE TABLE chromosome (
	id INTEGER NOT NULL,
	name CHARACTER VARYING(5) NOT NULL,
	length INTEGER,
	genetic_length INTEGER
);

INSERT INTO chromosome (
	id,
	name,
	length,
	genetic_length
)
SELECT
	id,
	name,
	length * 1000000,
	genetic_length
FROM
	tchromosome;


-- need to create an index on name
-- we already have one on id as it is the primary key

CREATE UNIQUE INDEX chromosome_name_idx
ON chromosome(name);

CREATE UNIQUE INDEX chromosome_id_idx
ON chromosome(id);

ALTER TABLE chromosome
ADD PRIMARY KEY USING INDEX chromosome_id_idx;

\echo 'BLOCK - end chromosome load'
-- --------------------------------------------------
-- Load the Gene list and index it
--

CREATE TABLE gene (
	id CHARACTER VARYING (18) NOT NULL,
	name CHARACTER VARYING (20),
	description CHARACTER VARYING (200),
	type CHARACTER VARYING (14)
);

COPY gene (
	id,
	name,
	description,
	type
)
FROM  '<<BASEDIR>>/data-for-import/genes.txt'
WITH (
	HEADER TRUE,
	FORMAT CSV
);

CREATE UNIQUE INDEX gene_id_idx
ON gene(id);

ALTER TABLE gene
ADD PRIMARY KEY USING INDEX gene_id_idx;

\echo 'BLOCK - end gene load'
-- --------------------------------------------------
-- Map the Genes to the Chromosomes
-- Here it is simplest to include the name into the table design then
-- backfill the chromosome index reference
--

CREATE TABLE gene_to_chromosome (
	chromosome_id INTEGER,
	chromosome_name CHARACTER VARYING(5),
	gene_id CHARACTER VARYING(18) NOT NULL,
	map_start INTEGER,
	map_end INTEGER,
	strand SMALLINT
);

COPY gene_to_chromosome (
	chromosome_name,
	gene_id,
	map_start,
	map_end,
	strand
)
FROM  '<<BASEDIR>>/data-for-import/genes-to-chromosomes.txt'
WITH (
	HEADER TRUE,
	FORMAT CSV
);



-- add in the chromosome id values by cross-referencing to the chromosome table

UPDATE gene_to_chromosome gtc
SET chromosome_id = c.id
FROM chromosome c
WHERE gtc.chromosome_name = c.name;


-- add indices on chromosome and gene id
CREATE INDEX gtc_chromosome_id_idx
ON gene_to_chromosome(chromosome_id);

CREATE INDEX gtc_gene_id_idx
ON gene_to_chromosome(gene_id);

\echo 'BLOCK - end map gene to chromosome'
-- --------------------------------------------------
-- Load the transcript ids and names
--

CREATE TABLE transcript (
	id CHARACTER VARYING(18) NOT NULL,
	name CHARACTER VARYING(25)
);

COPY transcript (
	id,
	name
)
FROM  '<<BASEDIR>>/data-for-import/transcripts.txt'
WITH (
	HEADER TRUE,
	FORMAT CSV
);

CREATE UNIQUE INDEX transcript_id_idx
ON transcript(id);

ALTER TABLE transcript
ADD PRIMARY KEY USING INDEX transcript_id_idx;

\echo 'BLOCK - end transcript load'
-- --------------------------------------------------
-- Map the transcripts to chromosomes
-- Again it is simplest to include the name into the table design then
-- backfill the chromosome index reference
--

CREATE TABLE transcript_to_chromosome (
	chromosome_id INTEGER,
	chromosome_name CHARACTER VARYING(5),
	transcript_id CHARACTER VARYING(18) NOT NULL,
	map_start INTEGER,
	map_end INTEGER,
	strand SMALLINT
);

COPY transcript_to_chromosome (
	transcript_id,
	chromosome_name,
	map_start,
	map_end,
	strand
)
FROM  '<<BASEDIR>>/data-for-import/transcripts-to-chromosomes.txt'
WITH (
	HEADER TRUE,
	FORMAT CSV
);

-- add in the chromosome id values by cross-referencing to the chromosome table

UPDATE transcript_to_chromosome ttc
SET chromosome_id = c.id
FROM chromosome c
WHERE ttc.chromosome_name = c.name;


-- add indices on chromosome and transcript id
CREATE INDEX ttc_chromosome_id_idx
ON transcript_to_chromosome(chromosome_id);

CREATE INDEX ttc_transcript_id_idx
ON transcript_to_chromosome(transcript_id);

\echo 'BLOCK - map transcript to chromosome'
-- --------------------------------------------------
-- Map transcripts to genes

CREATE TABLE transcript_to_gene (
	transcript_id CHARACTER VARYING(18),
	gene_id CHARACTER VARYING(18)
);

COPY transcript_to_gene (
	transcript_id,
	gene_id
)
FROM  '<<BASEDIR>>/data-for-import/transcripts-to-genes.txt'
WITH (
	HEADER TRUE,
	FORMAT CSV
);

\echo 'BLOCK - map transcript to gene'
-- --------------------------------------------------
-- Define the 5' UTR ends of the transcripts

CREATE TABLE fivep_utr (
	transcript_id CHARACTER VARYING(18),
	chromosome_id INTEGER,
	chromosome_name CHARACTER VARYING(5),
	map_start INTEGER,
	map_end INTEGER,
	strand SMALLINT
);

COPY fivep_utr (
	transcript_id,
	chromosome_name,
	strand,
	map_start,
	map_end
)
FROM  '<<BASEDIR>>/data-for-import/transcript-5prime-ends.txt'
WITH (
	HEADER TRUE,
	FORMAT CSV
);

-- add in the chromosome id values by cross-referencing to the chromosome table

UPDATE fivep_utr fu
SET chromosome_id = c.id
FROM chromosome c
WHERE fu.chromosome_name = c.name;


-- add indices on chromosome and transcript id
CREATE INDEX fu_chromosome_id_idx
ON fivep_utr(chromosome_id);

CREATE INDEX fu_transcript_id_idx
ON fivep_utr(transcript_id);

\echo 'BLOCK - end 5p UTR load'
-- --------------------------------------------------
-- Define the 3' UTR ends of the transcripts

CREATE TABLE threep_utr (
	transcript_id CHARACTER VARYING(18),
	chromosome_id INTEGER,
	chromosome_name CHARACTER VARYING(5),
	map_start INTEGER,
	map_end INTEGER,
	strand SMALLINT
);

COPY threep_utr (
	transcript_id,
	chromosome_name,
	strand,
	map_start,
	map_end
)
FROM  '<<BASEDIR>>/data-for-import/transcript-3prime-ends.txt'
WITH (
	HEADER TRUE,
	FORMAT CSV
);

-- add in the chromosome id values by cross-referencing to the chromosome table

UPDATE threep_utr tu
SET chromosome_id = c.id
FROM chromosome c
WHERE tu.chromosome_name = c.name;


-- add indices on chromosome and transcript id
CREATE INDEX tu_chromosome_id_idx
ON threep_utr(chromosome_id);

CREATE INDEX tu_transcript_id_idx
ON threep_utr(transcript_id);

\echo 'BLOCK - end 3p UTR load'
-- --------------------------------------------------
-- Load the exons

CREATE TABLE exon (
	id CHARACTER VARYING(18) NOT NULL
);

COPY exon (
	id
)
FROM  '<<BASEDIR>>/data-for-import/exons.txt'
WITH (
	HEADER TRUE,
	FORMAT CSV
);

CREATE UNIQUE INDEX e_id_idx
ON exon(id);

ALTER TABLE exon
ADD PRIMARY KEY USING INDEX e_id_idx;

\echo 'BLOCK - end exon load'
-- --------------------------------------------------
-- Map the exons to chromosomes
--

CREATE TABLE exon_to_chromosome (
	chromosome_id INTEGER,
	chromosome_name CHARACTER VARYING(5),
	exon_id CHARACTER VARYING(18) NOT NULL,
	map_start INTEGER,
	map_end INTEGER,
	strand SMALLINT
);


COPY exon_to_chromosome (
	exon_id,
	chromosome_name,
	strand,
	map_start,
	map_end
)
FROM  '<<BASEDIR>>/data-for-import/exons-to-chromosomes.txt'
WITH (
	HEADER TRUE,
	FORMAT CSV
);

-- add in the chromosome id values by cross-referencing to the chromosome table

UPDATE exon_to_chromosome etc
SET chromosome_id = c.id
FROM chromosome c
WHERE etc.chromosome_name = c.name;


-- add indices on chromosome and exon id
CREATE INDEX etc_chromosome_id_idx
ON exon_to_chromosome(chromosome_id);

CREATE INDEX etc_exon_id_idx
ON exon_to_chromosome(exon_id);

\echo 'BLOCK - end map exon to chromosome'
-- --------------------------------------------------
-- Map the exons to transcripts
-- 

CREATE TABLE exon_to_transcript (
	exon_id CHARACTER VARYING(18) NOT NULL,
	transcript_id CHARACTER VARYING(18) NOT NULL,
	rank SMALLINT
);

COPY exon_to_transcript (
	exon_id,
	transcript_id,
	rank
)
FROM  '<<BASEDIR>>/data-for-import/exons-to-transcripts.txt'
WITH (
	HEADER TRUE,
	FORMAT CSV
);


-- add indices on transcript and exon id
CREATE INDEX ett_transcript_id_idx
ON exon_to_transcript(transcript_id);

CREATE INDEX ett_exon_id_idx
ON exon_to_transcript(exon_id);

\echo 'BLOCK - end map exon to transcript'

-- Turn timing off (so we can process the output more easily)
\timing off

COMMIT;

:tsize
