-- 
-- Copyright (c) 2017 Andy Law
-- 				   (<s1578554@sms.ed.ac.uk>, <andy.law@roslin.ed.ac.uk>)
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
-- 
-- ----------------------------------------------- 
BEGIN;
-- --------------------------------------------------
-- SQL Script to load Gene Ontology and related data into PostgreSQL
--
-- The script assumes that the following data files have been placed
-- into a 'data-for-import' directory. Before running the script, it
-- must first be passed through a filter to replace the placeholder text
-- '<<BASEDIR>>' with the path to the location of the data-for-import
-- directory e.g.
--
--  cat postgres-load-go-terms.sql | sed -e "s#<<BASEDIR>>#/path/to/directory#"
--
--     go-terms.txt
--     go-is_a.txt
--     go-part_of.txt
--     go-aliases.txt
--     go-regulates.txt
-- --------------------------------------------------

-- Turn on Timing
\timing on

-- create an alias for simple sizing
\set tsize 'SELECT nspname || \'.\' || relname AS \"relation\", pg_relation_size(C.oid) AS "size" FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace) WHERE nspname NOT IN (\'pg_catalog\', \'information_schema\', \'pg_toast\') ORDER BY relation;'

-- --------------------------------------------------
-- Load in the Gene Ontology Terms, including aliases
--

CREATE TABLE go_term (
	id CHARACTER VARYING(10) PRIMARY KEY,
	description CHARACTER VARYING(1350),
	name CHARACTER VARYING(200),
	type CHARACTER VARYING (20)
);

COPY go_term (
	id,
	description,
	name,
	type
)
FROM  '<<BASEDIR>>/data-for-import/go-terms.txt'
WITH (
	HEADER FALSE,
	FORMAT CSV
);

CREATE INDEX go_term_type_idx
ON go_term(type);

\echo 'BLOCK - end go_term load'

-- --------------------------------------------------
-- Build the is_a graph links
--

CREATE TABLE go_is_a (
	id CHARACTER VARYING(10) NOT NULL,
	target CHARACTER VARYING (10) NOT NULL
);

COPY go_is_a (
	id,
	target
)
FROM  '<<BASEDIR>>/data-for-import/go-is_a.txt'
WITH (
	HEADER FALSE,
	FORMAT CSV
);


CREATE INDEX go_is_a_id_target_idx
ON go_is_a(id,target);

\echo 'BLOCK - end go_is_a load'

-- --------------------------------------------------
-- Build the is_alias_of graph links
--

CREATE TABLE go_alias (
	id CHARACTER VARYING(10) NOT NULL,
	target CHARACTER VARYING (10) NOT NULL
);

COPY go_alias (
	id,
	target
)
FROM  '<<BASEDIR>>/data-for-import/go-aliases.txt'
WITH (
	HEADER FALSE,
	FORMAT CSV
);


CREATE INDEX go_alias_id_target_idx
ON go_alias(id,target);

\echo 'BLOCK - end go_alias load'

-- --------------------------------------------------
-- Build the is_part_of graph links
--

CREATE TABLE go_part (
	id CHARACTER VARYING(10) NOT NULL,
	target CHARACTER VARYING (10) NOT NULL
);

COPY go_part (
	id,
	target
)
FROM  '<<BASEDIR>>/data-for-import/go-part_of.txt'
WITH (
	HEADER FALSE,
	FORMAT CSV
);


CREATE INDEX go_part_id_target_idx
ON go_part(id,target);

\echo 'BLOCK - end go_part load'

-- --------------------------------------------------
-- Build the regulates graph links
--

CREATE TABLE go_regulates (
	id CHARACTER VARYING(10) NOT NULL,
	target CHARACTER VARYING (10) NOT NULL,
	direction CHARACTER VARYING(20) NOT NULL
);

COPY go_regulates (
	id,
	target,
	direction
)
FROM  '<<BASEDIR>>/data-for-import/go-regulates.txt'
WITH (
	HEADER FALSE,
	FORMAT CSV
);


CREATE INDEX go_regulates_id_target_idx
ON go_regulates(id,target);

\echo 'BLOCK - end ge_regulates load'

-- Turn timing off (so we can process the output more easily)
\timing off

COMMIT;

:tsize
