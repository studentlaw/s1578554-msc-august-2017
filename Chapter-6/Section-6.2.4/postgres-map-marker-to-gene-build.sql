-- 
-- Copyright (c) 2017 Andy Law
-- 				   (<s1578554@sms.ed.ac.uk>, <andy.law@roslin.ed.ac.uk>)
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
-- 
-- ----------------------------------------------- 
BEGIN;
-- --------------------------------------------------
-- SQL script to map markers against gene, transcripts, exons and UTRs
--
-- The script assumes that the Gene Build and marker data have all been
-- loaded prior to running.
-- --------------------------------------------------

-- Turn on Timing
\timing on

-- create an alias for simple sizing
\set tsize 'SELECT nspname || \'.\' || relname AS \"relation\", pg_relation_size(C.oid) AS "size" FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace) WHERE nspname NOT IN (\'pg_catalog\', \'information_schema\', \'pg_toast\') ORDER BY relation;'

-- --------------------------------------------------
-- Index the locations of the Genes
--
CREATE INDEX gene_to_chromosome_positions_idx
ON gene_to_chromosome (chromosome_id, map_start, map_end);

-- --------------------------------------------------
-- Index the locations of the Transcripts
--
CREATE INDEX transcript_to_chromosome_positions_idx
ON transcript_to_chromosome (chromosome_id, map_start, map_end);

-- --------------------------------------------------
-- Index the locations of the Exons
--
CREATE INDEX exon_to_chromosome_positions_idx
ON exon_to_chromosome (chromosome_id, map_start, map_end);

-- --------------------------------------------------
-- Index the locations of the 5-prime UTRs
--
CREATE INDEX fiveputr_to_chromosome_positions_idx
ON fivep_utr (chromosome_id, map_start, map_end);

-- --------------------------------------------------
-- Index the locations of the 3-prime UTRs
--
CREATE INDEX threeputr_to_chromosome_positions_idx
ON threep_utr (chromosome_id, map_start, map_end);


-- --------------------------------------------------
-- Map Markers against the location of the Genes
--
CREATE TABLE marker_to_gene
AS
SELECT
	mtc.marker_name,
	gtc.gene_id
FROM
	marker_to_chromosome mtc,
	gene_to_chromosome gtc
WHERE
	mtc.chromosome_id = gtc.chromosome_id
AND
	mtc.position >= gtc.map_start
AND
	mtc.position <= gtc.map_end;

\echo 'BLOCK - mapped markers to genes'


-- --------------------------------------------------
-- Map Markers against the location of the Transcripts
-- (some markers might only map to a subset of transcripts for a gene)
--
CREATE TABLE marker_to_transcript
AS
SELECT
	mtc.marker_name,
	ttc.transcript_id
FROM
	marker_to_chromosome mtc,
	transcript_to_chromosome ttc
WHERE
	mtc.chromosome_id = ttc.chromosome_id
AND
	mtc.position >= ttc.map_start
AND
	mtc.position <= ttc.map_end;

\echo 'BLOCK - mapped markers to transcripts'

-- --------------------------------------------------
-- Map Markers against the extents of the Exons
-- Will allow us to pick out intronic markers - in gene but not in exon
--
CREATE TABLE marker_to_exon
AS
SELECT
	mtc.marker_name,
	etc.exon_id
FROM
	marker_to_chromosome mtc,
	exon_to_chromosome etc
WHERE
	mtc.chromosome_id = etc.chromosome_id
AND
	mtc.position >= etc.map_start
AND
	mtc.position <= etc.map_end;

\echo 'BLOCK - mapped markers to exons'


-- --------------------------------------------------
-- Map Markers against the extents of the UTRs
--
CREATE TABLE marker_to_utrs
AS
SELECT
	mtc.marker_name,
	fputr.transcript_id,
	CAST('5p' as character varying(2)) AS utr_end
FROM
	marker_to_chromosome mtc,
	fivep_utr fputr
WHERE
	mtc.chromosome_id = fputr.chromosome_id
AND
	mtc.position >= fputr.map_start
AND
	mtc.position <= fputr.map_end;

INSERT INTO marker_to_utrs (
	marker_name,
	transcript_id,
	utr_end
)
SELECT
	mtc.marker_name,
	tputr.transcript_id,
	'3p' AS utr_end
FROM
	marker_to_chromosome mtc,
	threep_utr tputr
WHERE
	mtc.chromosome_id = tputr.chromosome_id
AND
	mtc.position >= tputr.map_start
AND
	mtc.position <= tputr.map_end;

\echo 'BLOCK - mapped markers to UTRs'

-- Turn timing off (so we can process the output more easily)
\timing off

COMMIT;

:tsize
