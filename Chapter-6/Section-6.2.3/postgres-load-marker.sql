-- 
-- Copyright (c) 2017 Andy Law
-- 				   (<s1578554@sms.ed.ac.uk>, <andy.law@roslin.ed.ac.uk>)
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
-- 
-- ----------------------------------------------- 
BEGIN;
-- --------------------------------------------------
-- SQL Script to load Marker data from the simulation program output into PostgreSQL
--
-- The script assumes that the following data files have been placed
-- into a 'data-for-import' directory. Before running the script, it
-- must first be passed through a filter to replace the placeholder text
-- '<<BASEDIR>>' with the path to the location of the data-for-import
-- directory e.g.
--
--  cat postgres-load-markers.sql | sed -e "s#<<BASEDIR>>#/path/to/directory#"
--
--     markers.txt
-- --------------------------------------------------

-- Turn on Timing
\timing on

-- create an alias for simple sizing
\set tsize 'SELECT nspname || \'.\' || relname AS \"relation\", pg_relation_size(C.oid) AS "size" FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace) WHERE nspname NOT IN (\'pg_catalog\', \'information_schema\', \'pg_toast\') ORDER BY relation;'


-- --------------------------------------------------
-- Load the marker data and convert the genetic length to the nucleotide
-- position
-- The Chromosomes are indexed, not named here
--


CREATE TEMPORARY TABLE load_markers (
	name CHARACTER VARYING(8) NOT NULL,
	chromosome_id SMALLINT NOT NULL,
	position REAL NOT NULL
);

COPY load_markers (
	name,
	chromosome_id,
	position
)
FROM  '<<BASEDIR>>/data-for-import/markers.txt'
WITH (
	HEADER TRUE,
	FORMAT CSV
);


CREATE TABLE marker
AS
SELECT
	name
FROM
	load_markers;

CREATE UNIQUE INDEX marker_name_idx
ON marker(name);

ALTER TABLE marker
ADD PRIMARY KEY USING index marker_name_idx;

\echo 'BLOCK - end marker load'

CREATE TABLE marker_to_chromosome (
	marker_name CHARACTER VARYING(8) NOT NULL PRIMARY KEY,
	chromosome_id SMALLINT NOT NULL,
	position INTEGER NOT NULL
);

-- --------------------------------------------------
-- Index the locations of the Markers
--
CREATE INDEX marker_to_chromosome_id_pos_idx
ON marker_to_chromosome(chromosome_id, position);

-- Insert the Marker locations

INSERT INTO  marker_to_chromosome (
	marker_name,
	chromosome_id,
	position
)
SELECT
	lm.name,
	lm.chromosome_id,
	CAST ( ( lm.position / c.genetic_length ) * c.length AS INTEGER)
FROM
	load_markers lm,
	chromosome c
WHERE
	c.id = lm.chromosome_id;


\echo 'BLOCK - end mapping marker to chromosome'

-- Turn timing off (so we can process the output more easily)
\timing off

COMMIT;

:tsize
