# Pre-processing pedigree data

Pedigree and phenotype data were converted using awk with the following command:

```
cat p2_data_001.txt | \
	awk 'BEGIN {OFS=","} \
		$5 == 0 {$2 = ""; $3 = ""} \
		{print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13}' > pedigree.txt
```
